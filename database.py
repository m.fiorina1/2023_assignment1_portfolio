from sqlite3 import connect


class DB:
    def __init__(self):
        self.database = connect("database.db", 10.0, check_same_thread=False)

    def get_progetti(self) -> list[dict[str, str]]:
        """Richiede i progetti dal database."""
        data = self.database.cursor().execute("SELECT * FROM progetti")
        names = ["title", "description", "url", "tech"]
        return [{names[i]: x[i] for i in range(len(names))} for x in data]
