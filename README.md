# 2023_assignment1_portfolio

Matteo Fiorina 865956\
Tommaso Ferrario 869005\
Simone Vendramini 866229

Repository: https://gitlab.com/m.fiorina1/2023_assignment1_portfolio

Docs: https://2023-assignment1-portfolio-m-fiorina1-fe460b8014f8adfb1045d5cdb.gitlab.io/

## Progetto
Questo progetto consenta la visualizzazione di collegamenti a progetti universitari. 
I progetti sono salvati in un database sqlite e serviti tramite una backend implementata
con il pacchetto python Flask.

## Pipeline
Prima di iniziare con l'esecuzione degli stage viene creato e attivato un virtual environment. Oltre a questo viene creata una cache 
per condividere i pacchetti installati tra i vari job.

Per lo sviluppo della pipeline abbiamo seguito l'esempio fornito:
* build: pip per installare i requisiti
* verify: prospector e bandit per l'analisi statica
* test: pytest per unit test e per integration test
* package: setuptool e wheel per creare i package
* release: twine per pubblicare su pypi (abbiamo utilizzato l'index di test)
* docs: MkDocs per generare la documentazione a partire da una file markdown

### Build - job
Nello stage di build vengono installati i pacchetti necessari per l'esecuzione del progetto, specificati nel fine "requirements.txt".\
Viene inoltre eseguito il file "init_database.py" grazie al quale viene creato e inizializzato un database sqlite nel file "database.db".

### Verify - job
È costituito dall'esecuzione di due job di verifica (uno per Bandit e uno per Prospector) eseguiti in parallelo.\
Per quanto riguarda l'utilizzo di prospector è esclusa la cartella _venv/_ per evitare di analizzare il virtual environment.

### Tests - job
Utilizzando pytest vengono eseguiti, in due stage differenti i test di unità e di integrazione.
* Test di unità che controlla se il comando get_progetti riceve correttamente la lista dei progetti.
* Test d'integrazione che verifica il corretto collegamento con l'endpoint.

### Package - job
Il package viene creato tramite setuptools e wheel, i quali utilizzano il file "pyproject.toml" per ottenere informazioni utili sulla creazione del pacchetto.\
Il risultato della loro invocazione viene salvato nella cartella "dist".\
La versione viene determinata utilizzando l'estensione setuptools-scm di setuptools, il quale legge l'ultimo git tag.\
In questo modo risulta possibile pubblicare una nuova versione solo quando lo si ritiene necessario.

### Release - job
Utilizzando twine viene pubblicato il package creato nel job precedente su PyPi (in questo caso abbiamo pubblicato su Test PyPi).\
Se la versione associata con l'ultima tag è già stata pubblicata su PyPi, il pacchetto creato non viene pubblicato.

### Docs - job
La documentazione relativa al progetto viene scritta staticamente in "docs/index.md" e successivamente, utilizzando MkDocs, viene generata un'interfaccia web per la sua visualizzazione.\
Utilizzando la keyword "pages" per il job, l'interfaccia creata durante l'esecuzione di esso viene salvata nella cartella "public" e viene automaticamente caricata sulle gitlab pages.
