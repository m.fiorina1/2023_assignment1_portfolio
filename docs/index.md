# Documentazione

Il programma consente la visualizzazione di collegamenti a progetti universitari.

#### Database

Il file database.py gestisce le interazioni tra programma e database.

Presenta i seguenti metodi

* get_progetti: _Richiede i progetti dal database._

#### Test_sample

Il file test_sample.py contiene una serie di test per verificare l'integrazione e vari comandi del programma.

Presenta i seguenti metodi

* test_answer: _Unit Test che controlla se il comando get_progetti riceve correttamente la lista dei progetti._
* test_integration: _Test d'integrazione che verifica il corretto collegamento con l'endpoint_

#### App

Il file app.py contiene i vari endpoint del programma.

Presenta i seguenti endpoint

* /: _Route principale del programma._
* api/progetti: _Endpoint per recuperare i progetti dal database_