import sqlite3

c = sqlite3.connect("database.db")

with open("create.sql", encoding="utf-8") as f:
    c.executescript(f.read())
    c.commit()
