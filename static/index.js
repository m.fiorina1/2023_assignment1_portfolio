const bio = Vue.createApp({
    data() {
        return {
            bio: "Hi I'm a computer science student at the University of Milano-Bicocca. In July 2023, I received my bachelor's degree in computer science. In the coming years, I will conclude my course of study by earning a master's degree. During this time I will deepen my skills in the field of artificial intelligence and data analysis.",
        }
    }
})

bio.mount("#bio")

const project = Vue.createApp({
    data() {
        return {
            projects: null
        }
    },
    async created() {
        fetch("api/progetti")
            .then((response) => response.json())
            .then((json) => { this.projects = json })
    },
    methods: {
    }
})

project.mount("#project")

const cont = Vue.createApp({
    data() {
        return {
            linkedin: "https://www.linkedin.com/in/tommaso-ferrario-383423200/",
            github: "https://github.com/TommasoFerrario18"
        }
    }
})

cont.mount("#contacts")
