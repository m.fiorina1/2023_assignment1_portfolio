from flask import Flask, send_file
from flask_restful import Api, Resource

from database import DB

app = Flask(__name__)
api = Api(app, prefix="/api/")

db = DB()


@app.route("/")
def index():
    """Route principale del programma."""
    return send_file("static/index.html")


@api.resource("progetti")
class DatabaseResource(Resource):
    """Endpoint per recuperare i progetti dal database"""
    def get(self):
        data = db.get_progetti()
        for entry in data:
            entry["class"] = "carousel-item"
        data[0]["class"] += " active"
        return data
