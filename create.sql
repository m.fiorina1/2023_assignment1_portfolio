DROP TABLE IF EXISTS progetti;

CREATE TABLE progetti(
  title Text NOT NULL PRIMARY KEY,
  description Text NOT NULL,
  url Text NOT NULL,
  tech Text NOT NULL
);

INSERT INTO progetti (title, description, url, tech)
VALUES (
    'Parsing di stringhe URI',
    'Progetto di Linguaggi di Programmazione Progetto Lisp e Prolog Gennaio-Febbraio 2022 Università degli studi Milano-Bicocca',
    'https://github.com/TommasoFerrario18/Parsing-di-stringhe-URI',
    'Common Lisp - Prolog'
);

INSERT INTO progetti (title, description, url, tech)
VALUES (
    'Tool Recognition',
    'Applicazione che, data una immagine permetta di riconoscere un set di utensili meccanici posizionati su un piano.',
    'https://github.com/Svendra4UniMiB/ToolRecognition',
    'matlab'
);

