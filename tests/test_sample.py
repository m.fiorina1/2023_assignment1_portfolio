from app import app
from database import DB


# unit_test
def test_unit():
    """Unit Test che controlla se il comando get_progetti riceve correttamente la lista dei progetti."""
    db = DB()
    progetti = db.get_progetti()
    assert isinstance(progetti, list)
    keys = set(["title", "description", "url", "tech"])
    for p in progetti:
        assert keys == p.keys()


# integration test
def test_integration():
    """Test d'integrazione che verifica il corretto collegamento con l'endpoint"""
    # Run flask
    with app.test_client() as test_client:
        # Endpoint
        path = "http://127.0.0.1:5000/api/progetti"

        # request
        data = test_client.get(path)
        assert data.status_code == 200
        assert data.text != ""
        assert (
            '{"title": "Tool Recognition", "description": "Applicazione che, data una immagine permetta di riconoscere un set di utensili meccanici posizionati su un piano.", "url": "https://github.com/Svendra4UniMiB/ToolRecognition", "tech": "matlab", "class": "carousel-item"}'
            in data.text
        )
