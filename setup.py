from setuptools import setup


def no_local_version():
    from setuptools_scm.version import SEMVER_MINOR, guess_next_simple_semver

    return {
        "version_scheme": lambda version: guess_next_simple_semver(
            version, retain=SEMVER_MINOR, increment=False
        ),
        "local_scheme": "no-local-version",
    }


setup(
    use_scm_version=no_local_version,
    setup_requirements=[
        "setuptools",
        "Flask==2.1.2",
        "Werkzeug==2.2.2",
        "Flask-RESTful==0.3.9",
        "setuptools-scm==8.0.4",
    ],
    name="portfolio_unimib_2023",
)
